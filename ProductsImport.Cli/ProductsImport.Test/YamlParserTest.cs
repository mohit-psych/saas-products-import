﻿using Newtonsoft.Json;
using ProductsImport.Cli.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ProductsImport.Test
{
    public class YamlParserTest
    {
        private readonly YamlParser _sut;
        private readonly MockData _mockData;

        public YamlParserTest()
        {
            _sut = new YamlParser();
            _mockData = new MockData();
        }

        [Fact]
        public void ParseProductsShouldParseProductsByReadingFromYamlFile()
        {
            var mockProducts = JsonConvert.SerializeObject(_mockData.GetYamlMockProducts());
            var actualProducts = JsonConvert.SerializeObject(_sut.ParseProducts("feed-products/capterra.yaml"));
            Assert.Equal(mockProducts, actualProducts);
        }
    }
}
