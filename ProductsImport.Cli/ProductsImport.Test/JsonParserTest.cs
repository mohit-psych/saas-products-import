using Newtonsoft.Json;
using ProductsImport.Cli.Utility;
using System;
using Xunit;

namespace ProductsImport.Test
{
    public class JsonParserTest
    {
        private readonly JsonParser _sut;
        private readonly MockData _mockData;

        public JsonParserTest()
        {
            _sut = new JsonParser();
            _mockData = new MockData();
        }

        [Fact]
        public void ParseProductsShouldParseProductsByReadingFromJsonFile()
        {
            var mockProducts = JsonConvert.SerializeObject(_mockData.GetMockProducts());
            var actualProducts = JsonConvert.SerializeObject(_sut.ParseProducts("feed-products/softwareadvice.json"));
            Assert.Equal(mockProducts, actualProducts);
        }
    }
}
