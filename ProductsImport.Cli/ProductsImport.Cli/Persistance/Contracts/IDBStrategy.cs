﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsImport.Cli.Persistance.Contracts
{
    public interface IDBStrategy
    {
        public void GetDBClient();
    }
}
