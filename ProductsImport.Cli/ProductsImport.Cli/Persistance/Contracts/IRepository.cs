﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsImport.Cli.Persistance.Contracts
{
    public interface IRepository
    {
        public void Add();
        public void Get();
        public void Update();
        public void Delete();
    }
}
