﻿using ProductsImport.Cli.Contratcs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsImport.Cli.Utility
{
    public class ParserFactory
    {
        public IParser GetParser(string parserName)
        {
            switch(parserName)
            {
                case "yaml":
                    return new YamlParser();
                case "json":
                    return new JsonParser();
            }
            return null;
        }
    }
}
