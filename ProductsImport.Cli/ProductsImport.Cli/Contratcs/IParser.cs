﻿using ProductsImport.Cli.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsImport.Cli.Contratcs
{
    public interface IParser
    {
        public IEnumerable<Product> ParseProducts(string path);
    }
}
