# SaaS Products Import Tool



Installation steps(CLI commands):

- cd ~\ProductsImport.Cli\ProductsImport.Cli
- dotnet tool install --global --add-source ./nupkg ProductsImport.Cli


How to run the tool(CLI commands)?

- cd ~\ProductsImport.Cli\ProductsImport.Cli
- import capterra feed-products/capterra.yaml


How to run the tests(CLI commands)?

- cd ~\ProductsImport.Cli\ProductsImport.Test
- dotnet test


Where to find the code?
https://gitlab.com/mohit-psych/saas-products-import.git


Answers for SQL Test Assignment are present in **sql_assigment.txt** file
